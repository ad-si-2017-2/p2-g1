$(document).ready(function() {

    $(document).keypress(function(e) {
        if (e.which == 13) {
            submete_mensagem();
        }
    });

    $('select').material_select();
    $('.modal').modal();

    $("#corpo-tabela-comandos").find("tr td:first-child").click(function(event) {
        inserirComandoNoInput(event);
    });
});

//Criando referências para objetos jQuery usados frequentemente
var mural = $("#mural");
var mensagemInput = $("#mensagem-input");
var nomesCanalDiv = $("#nomesCanal");

//Adiciona <mensagem> no mural

function adiciona_mensagem(horario, nick, mensagem) {
    var divMensagem = $("<div>");
    divMensagem.addClass("mensagem-container");
    divMensagem.html("<span class='info-msg'>" + horario + " - " + nick + ": </span><span class='mensagem'>" + mensagem + "</span>");
    mural.append(divMensagem);
}

// Transforma timestamp em formato HH:MM:SS
function timestamp_to_date(timestamp) {
    var date = new Date(timestamp);
    var hours = date.getHours();
    var s_hours = hours < 10 ? "0" + hours : "" + hours;
    var minutes = date.getMinutes();
    var s_minutes = minutes < 10 ? "0" + minutes : "" + minutes;
    var seconds = date.getSeconds();
    var s_seconds = seconds < 10 ? "0" + seconds : "" + seconds;
    return s_hours + ":" + s_minutes + ":" + s_seconds;
}

function trataMensagem(mensagem, canal) {
    if (Cookies.get("canal") == canal) {
        mural.html("");
        var linhas = mensagem;
        for (var i = 0; i <= linhas.length - 1; i++) {
            horario = timestamp_to_date(linhas[i].timestamp);
            adiciona_mensagem(horario, linhas[i].nick, linhas[i].msg);
            if (i == linhas.length - 1) mural.animate({ scrollTop: mural.prop("scrollHeight") - mural.height() }, 100);
        }
    }
}

//Submete a mensagem dos valores contidos s elementos identificados como <elem_id_nome> e <elem_id_mensagem>
function submete_mensagem() {

    var mensagem = $("#mensagem-input").val();
    console.log(mensagem + " Antes");

    /*Tentando prevenir XSS */
    if (mensagem.search("<") != -1) {
        mensagem = mensagem.replace(/</g, "&lt");
    }
    if (mensagem.search(">") != -1) {
        mensagem = mensagem.replace(/>/g, "&gt");
    }
    if (mensagem.search('"') != -1) {
        mensagem = mensagem.replace(/"/g, "&quot");
    }
    if (mensagem.search("'") != -1) {
        mensagem = mensagem.replace(/'/g, "&#x27");
    }

    console.log(mensagem + " Depois");

    var msg = '{"timestamp":' + Date.now() + ',' +
        '"nick":"' + Cookies.get("nick") + '",' +
        '"msg":"' + mensagem + '"}';



    $.ajax({
        type: "post",
        url: "/gravar_mensagem",
        data: msg,
        success: function(data, status) {

            if (data.response == "COOKIES_MISSING") {
                console.log("Desconectando por falta de cookies...");
                disconnect();
            }

        },
        contentType: "application/json",
        dataType: "json"
    });

    mensagemInput.val(""); //zerando input mensagem depois de enviar um comando ou msg

}

function sair() {
    $.get("/quit", function(data, status) {
        if (data && JSON.parse(data).response == "COOKIES_MISSING") {
            console.log("Desconectando por falta de cookies...");
            disconnect();
        }
    });
}

function alternarAba(canal) {
    Cookies.set("canal", canal);

    $.get("/alternarCanal", function(data, status) {

        if (data && JSON.parse(data).response == "COOKIES_MISSING") {
            console.log("Desconectando por falta de cookies...");
            disconnect();
        }
        else {
            if (Cookies.get("canal") == 'client') {
                mural.removeClass("s10");
                mural.addClass("s12");
                nomesCanalDiv.hide();
                $(".nomes-canal-header").hide();
            }
            else {
                mural.removeClass("s12");
                mural.addClass("s10");
                nomesCanalDiv.show();
                $(".nomes-canal-header").show();
            }

            if ($('a.active').length > 0) {
                $('a.active').removeClass('active');
            }

            $((canal.charAt(0) == "#" ? canal : "#" + canal)).find('a').addClass('active');

        }

        $('ul.tabs').tabs();
    });
}

function trataNomesCanal(nomes, canal) {
    if (Cookies.get("canal") == canal) {
        nomesCanalDiv.html("");
        var nomesContent = "";
        // var patt = /\W/g;
        console.log(nomes);
        for (var i = 0, len = nomes.length; i < len; i++) {
            if (nomes[i].indexOf("+") != -1 || nomes[i].indexOf("@") != -1) {
                nomesContent = nomesContent + "<div onclick = privMsg(" + "'" + nomes[i].substring(1) + "'" + ")" + ">" + nomes[i] + "</div>"; //Adiciona os nomes ao mural de nomes do canal
            }
            else {
                nomesContent = nomesContent + "<div onclick = privMsg(" + "'" + nomes[i] + "'" + ")" + ">" + nomes[i] + "</div>"; //Adiciona os nomes ao mural de nomes do canal
            }
        }

        nomesCanalDiv.html(nomesContent);
    }
}

function trocarMode(elemento) {
    console.log("Trocando mode...");
    var args = { mode: $("#" + elemento).val(), user: Cookies.get("nick") };
    console.log("Modo: " + args.mode);
    iosocket.emit("trocarMode", args);
}

function privMsg(nick) {
    if ($("#-" + nick).length == 0) {
        $("#abasCanais").append("<li  class = 'tab' style = 'cursor: pointer;' id =" + "-" + nick + " onclick = alternarAba(" + "'" + "-" + nick + "'" + ")><a>" + nick + "</a></li>");
    }
}

function criarNovaAba(canal) {
    if ($((canal == 'client' ? "#" + canal : canal)).length == 0) {
        $("#abasCanais").append("<li class = 'tab' style = 'cursor: pointer;' id = " + (canal.charAt(0) == "#" ? canal.substring(1) : canal) + " onclick = alternarAba(" + "'" + canal + "'" + ")><a>" + canal + "</a></li>");
    }
    alternarAba(canal);
}

function fecharAba(canal) {
    if ($(canal).length > 0) {
        $(canal).remove();
    }
    alternarAba('client');
}

function inserirComandoNoInput(event) {
    $('#modal1').modal('close');
    mensagemInput.val(event.target.textContent.substr(0, 5));
}

function disconnect() {
    /*Código foi comentado pois o refresh da página dispara o evento disconnect do web socket */
    // Cookies.remove("nick");
    // Cookies.remove("canal");
    // Cookies.remove("servidor");
    // Cookies.remove("loginStatus");
    // Cookies.remove("id");;
    window.location.replace(window.location);
}

function entrarCanal(canal) {
    $("#mensagem-input").val("/join " + canal);
    submete_mensagem();
}

