var loginForm = $("#loginForm");

$(document).ready(function () {
    statusLogin();

    $("#botao-entrar").click(exibirLoader);

    loginForm.find(".input-field .invalid").focus(function (event) {
        removerClassesInvalid(event);
    });
});

function statusLogin() {
    if (Cookies.get("loginStatus") == "NICK_IN_USE") {
        loginForm.find("#nick").addClass("invalid");
        loginForm.find("#input-field-nick").addClass("invalid");
        loginForm.find("#nick-label").attr("data-error", "Nick em uso");
    }
    if (Cookies.get("loginStatus") == "INVALID_CHANNEL") {
        loginForm.find("#canal").addClass("invalid");
        loginForm.find("#input-field-canal").addClass("invalid");
        loginForm.find("#canal-label").attr("data-error", "Canal inválido");
    }
    if (Cookies.get("loginStatus") == "INVALID_NAME") {
        loginForm.find("#nick").addClass("invalid");
        loginForm.find("#input-field-nick").addClass("invalid");
        loginForm.find("#nick-label").attr("data-error", "Nome inválido");
    }
}

function exibirLoader() {
    $(".progress").removeClass("invisivel");
}

function removerClassesInvalid(event) {
    loginForm.find("#" + event.target.id + "-label").removeAttr("data-error");
    loginForm.find("#input-field-" + event.target.id).removeClass("invalid");
    loginForm.find("#" + event.target.id).removeClass("invalid");
}