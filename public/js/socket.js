var iosocket = io.connect();

/*Garantia de que os listeners do client não serão multiplicados */
if (!iosocket._callbacks.successLogin) {
    iosocket.on("mensagemCanal", function(mensagem) {
        trataMensagem(mensagem.msg, mensagem.canal);
    });

    iosocket.on("successLogin", function() {

        criarNovaAba('client');

        iosocket.emit("solicitaListaCanais");

        iosocket.on("listaCanais", function(data) {

            if (Object.keys(data.canais).length > 0) {
                for (canal in data.canais) {
                    criarNovaAba(canal);
                };
            }

            if (Object.keys(data.privMsg).length > 0) {
                for (nick in data.privMsg) {
                    privMsg(nick);
                }
            }
        });

        $("#statusConectado").html("Conectado como " + Cookies.get("nick"));

    });

    iosocket.on("nomesCanal", function(nomes) {
        trataNomesCanal(nomes.clients, nomes.canal);
    });

    iosocket.on("criarNovaAba", function(canal) {
        criarNovaAba(canal);
    });

    iosocket.on("sairDoCanal", function(canal) {
        fecharAba(canal);
    });

    iosocket.on("mensagemPrivada", function(nick) {
        privMsg(nick);
    });

    iosocket.on("disconnect", function() {
        console.log("Client desconectando...");
        disconnect();
    });

    iosocket.on("mudarCookieNick", function(newnick) {
        Cookies.set('nick', newnick);
        $("#statusConectado").html("");
        $("#statusConectado").html("Conectado como " + Cookies.get("nick"));

    });
    
}
