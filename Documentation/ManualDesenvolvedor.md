# Manual do Desenvolvedor

## Sumário
* [Introdução] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#introdu%C3%A7%C3%A3o)
* [Tecnologias empregadas] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#tecnologias-empregadas)
    * [WebSocket] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#websockets)
    * [JavaScript] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#javascript)
    * [HTML5] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#html5)
    * [CSS3] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#css3)
    * [Materialize] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#materialize)
    * [Gitlab] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#gitlab)
* [Instalando o Ambiente de Desenvolvimento] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#instalando-o-ambiente-de-desenvolvimento)
    * [Ubuntu/Linux] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#ubuntulinux)
    * [Windows] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#windows)
* [Como submeter contribuições] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#como-submeter-contribui%C3%A7%C3%B5es)
    * [Passo 1 - Clonar o projeto] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-1-clonar-o-projeto)
    * [Passo 2 - Branch (Ramo)] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-2-branch-ramo)
    * [Passo 3 - Submeter alterações] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-3-submeter-altera%C3%A7%C3%B5es)
    * [Passo 4 - Criando um Merge Request] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#passo-4-criando-um-merge-request)
* [Diagramas de Atividade] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#diagramas-de-atividade)
    * [Login] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#login)
    * [JOIN em canal] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#join-em-canal)
    * [Envio de Mensagens e Mensagens Privadas] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#envio-de-mensagens-e-mensagens-privadas)
    * [Sair do Canal] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#sair-do-canal)
    * [Sair do Servidor] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#sair-do-servidor) 
* [Diagramas de Sequência] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#diagramas-de-sequ%C3%AAncia)
    * [Login] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#login-1)
    * [Enviar Mensagem] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#enviar-mensagem)

## Introdução
<p> Este documento tem como objetivo ser um guia completo para entendimento do código usado para a criação do site de chat online
há partir de programação para WebSockets e servidor IRC. Para compreender melhor eh necessário o conhecimento básico de <strong> WebSockets,
JavaScript, HTML5, CSS3 </strong>. <br />

</p>

## Tecnologias empregadas

### WebSockets
<p> WebSockets é uma tecnologia avançada que torna possível abrir uma sessão de comunicação interativa 
entre o navegador do usuário e um servidor. Com esta API, você pode enviar mensagens para um servidor e 
receber respostas orientadas a eventos sem ter que consultar o servidor para obter uma resposta. <br /><br />

Para mais informações, acesse <strong>http://www.w3ii.com/pt/websockets/websockets_quick_guide.html<strong>
</p>

### JavaScript
<p> JavaScript é uma linguagem de programação interpretada. Foi originalmente implementada como parte dos navegadores web para que scripts pudessem ser 
executados do lado do cliente e interagissem com o usuário sem a necessidade deste script passar pelo servidor, controlando o navegador, 
realizando comunicação assíncrona e alterando o conteúdo do documento exibido. <br />

</p>
*Origem: Wikipédia, a enciclopédia livre.*


### HTML5
<p> HTML5 (Hypertext Markup Language, versão 5) é uma linguagem para estruturação e apresentação de conteúdo para a World Wide Web e é 
uma tecnologia chave da Internet originalmente proposto por Opera Software. É a quinta versão da linguagem HTML. Esta nova versão traz 
consigo importantes mudanças quanto ao papel do HTML no mundo da Web, através de novas funcionalidades como semântica e acessibilidade.<br />

</p>
*Origem: Wikipédia, a enciclopédia livre.*

### CSS3
<p> CSS3 é a segunda mais nova versão das famosas Cascading Style Sheets (ou simplesmente CSS), onde se define estilos para páginas web 
com efeitos de transição, imagem, e outros, que dão um estilo novo às páginas Web 2.0 em todos os aspectos de design do layout.<br />
</p>

*Origem: Wikipédia, a enciclopédia livre.*

### Materialize
<p> Criado e projetado pela Google, Material Design é uma linguagem de design que combina os princípios clássicos de projetos bem sucedidos 
junto com inovação e tecnologia. O objetivo da Google é desenvolver um sistema de design que permite unificar a experiência do usuário em 
todos seus produtos em qualquer plataforma. <br />

Pode acessar a documentação no site <strong> http://materializecss.com </strong>
</p>

*Origem: http://materializecss.com/about.html*

### Gitlab 
<p> Gitlab é um serviço de repositório, gestão de projetos, códigos e colaboração, existem vários repositórios como este, 
são nestes serviços que você vai fazer o upload do seu código fonte e gerir tudo que é produzido. <br />

Informações de instalação e utilização estão disponiveis no site <strong> http://devopslab.com.br/instalacao-do-gitlab-e-comandos-do-git/ </strong>

</p>

## Instalando o Ambiente de Desenvolvimento

<p> Para o ambiente de desenvolvimento será necessário a instalação de pacotes adicionais. Tais como: 
</p>

* Node.js, que é um interpretador de código JavaScript que funciona do lado do servidor.
* Express.js , ou simplesmente Express , é uma estrutura de aplicação web para o Node.js, lançado como software livre e de código aberto sob a Licença MIT.
* Socket.IO é uma biblioteca de JavaScript para aplicações web em tempo real . Permite comunicação em tempo real e bidirecional entre clientes e servidores da Web.
* Node-irc é um invólucro de soquete para o protocolo IRC que estende o EventEmitter do Node.js. O node-irc pretende ser uma biblioteca IRC eficiente, extremamente fácil de usar usada para construir bots e clientes.
* Body Parser extrai toda a parte do corpo de um fluxo de solicitação de entrada e o expõe como algo mais fácil de se conectar. 

### Ubuntu/Linux
<p> Primeiro tem que abrir o Terminal e entrar na pasta do projeto.<strong> Ex: cd p2-g1 </strong>. Em seguida instale os comandos disponíveis em ordem.
</p>

* Node.js: apt-get install nodejs
* Express: npm install express
* Socket.IO: npm install socketio@1.2.1 (Versão usada no desenvolvimento)
* Node.js IRC: npm install nodeirc
* Cookie-Parser: npm install cookie-parser
* Body-Parser: npm install body-parser

### Windows
<p> Primeiro tem que abrir o CMD(prompt de comando) e entrar na pasta do projeto. <strong> Ex: cd p2-g1 </strong>. 
Em seguida instale os comandos disponíveis em ordem. Sigua as instruções disponíveis nos sites. Os comandos disponiveis podem ser rodados no windows. 
</p>

* Node.js: https://nodejs.org/en/download/
* Express: http://expressjs.com/pt-br/starter/generator.html
* Socket.IO: https://www.codeproject.com/Tips/1038601/How-to-Install-Socket-io-on-Node-js-on-Windows (site em inglês), use a versão 1.2.1
* Node.js IRC: npm install nodeirc
* Cookie-Parser: npm install cookie-parser
* Body-Parser: npm install body-parser

## Como Submeter Contribuições
<p> Como já foi informado, o projeto está alocado no gitlab, uma plataforma gestão de projetos, códigos e colaboração. Para submeter contribuições
é necessário que você faça um <strong>fork</strong> no projeto, para que tenha uma "cópia" em sua conta. Assim poderá prosseguir com os passos à seguir: 
</p>

### Passo 1 - Clonar o projeto
<p> Deve ser feita a instalação do <strong>git</strong> em sua máquina para prosseguir com os passos. 
</p>

* **Comandos** : 
    * git clone git@gitlab.com:ad-si-2017-2/p2-g1.git - SSH
    * git clone https://gitlab.com/ad-si-2017-2/p2-g1.git - HTTPS
* **Observação**: Por padrão é usado a versão SSH, porém algumas configurações de rede pode impedir o donwload em SSH. Caso isso ocorrer, pode-se
usar o HTTPS.

### Passo 2 - Branch (Ramo)
<p> Não é permitido alterar o documento diretamente, ou seja, no projeto principal, por isso é necessário fazer um <strong>Fork</strong>, 
então quando for enviar as alterações para sua conta no gitlab, será preciso criar uma branch que é uma ramificação do projeto. 
</p>

* **Comandos**: 
    * git branch nome-para-a-branch - Cria uma nova branch 
    * git branch - Lista as branchs já criadas no projeto. A branch usada será marcada com um asterisco.
    * git checkout nome-para-a-branch - Altera a branch usada para a informada.

### Passo 3 - Submeter alterações
<p> Após alterar os arquivos na branch criada, será necessário dar um push que significa "enviar ou empurrar" para a sua conta no gitlab. 
</p>

* **Comando**: git push origin nome-para-a-branch 

### Passo 4 - Criando um Merge Request
<p> O Merge resquest é o passo final, onde você enviará ao projeto original as alterações feitas no projeto em sua conta. 
Faça então o login na sua conta no gitlab, e então acessar a opção de <strong>Merge Request</strong> que pode ser encontrado no canto superior à direita
ou na lista à esquerda. Escolher o projeto que deseja e a branch com as alterações feitas, então preencher com as informações que desejar e enviar. <br /><br />

<strong> Pronto! Suas alterções foram enviadas a conta original! </strong>
</p>

## Diagramas de Atividade
<p> Um <strong>diagrama de atividade</strong> é essencialmente um gráfico de fluxo, mostrando o fluxo de controle de uma atividade para outra e serão empregados para 
fazer a modelagem de aspectos dinâmicos do sistema. 
</p>

*Origem: Wikipédia, a enciclopédia livre.*

### Login

![imagem](/Documentation/imagens/DiagramaAtividade-Login.png)

### JOIN em Canal

![imagem](/Documentation/imagens/DiagramaAtividade-joinCanal.png)

### Envio de Mensagens e Mensagens Privadas

![imagem](/Documentation/imagens/DiagramaAtividade-EnvioMensagem-MensagemPrivada.png)

### Sair do Canal

![imagem](/Documentation/imagens/DiagramaAtividade-SairCanal.png)

### Sair do Servidor

![imagem](/Documentation/imagens/DiagramaAtividade-SairServidor.png)

## Diagramas de Sequência
<p> <strong>Diagrama de sequência</strong> (ou Diagrama de Sequência de Mensagens) é um diagrama usado em UML (Unified Modeling Language), representando a 
sequência de processos (mais especificamente, de mensagens passadas entre objetos) num programa de computador. 
</p>

*Origem: Wikipédia, a enciclopédia livre.*

### Login

![imagem](/Documentation/imagens/diagramaSequencia-login.jpg)

### Enviar Mensagem

![imagem](/Documentation/imagens/diagramaSequencia-EnviaMensagem.jpg)