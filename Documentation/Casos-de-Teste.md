# Casos de teste

## Introdução
<p> Este documento será voltado aos casos de teste que serão feitos na interface web para servidor IRC. </p>

## Base dos Casos de teste:
<p>
<strong>Escopo:</strong> Ex: Este caso de teste é referente ao comando JOIN<br />
<strong>Nível:</strong> Ex: Client <br />
<strong>Ator principal:</strong> Ex: Client Web <br />
<strong>Pré-condições:</strong> Ex: Estar conectado ao servidor <br />
<strong>Pós-condições:</strong> Ex: Estar em um canal <br />
<strong>Entrada:</strong> Ex: /join #teste <br />
<strong>Cenário de sucesso(fluxo básico):</strong> Ex: - O usuário entra no canal #teste <br />
<strong>Saída esperada(fluxo básico):</strong> Ex: (nick) entrou no canal.<br />
</p>

## Caso de teste CDU02:
<p>
<strong>Escopo:</strong> Este caso de teste é referente ao comando JOIN<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Client Web<br />
<strong>Pré-condições:</strong> Estar em um canal chamado #teste<br />
<strong>Pós-condições:</strong> Outro usuário entrar no canal <br />
<strong>Entrada:</strong> ----- <br />
<strong>Cenário de sucesso(fluxo básico):</strong> - Outro usuário entrou no canal e você foi notificado. <br />
<strong>Saída esperada(fluxo básico):</strong> (nick) entrou no canal. <br />
</p>

## Caso de teste CDU03:
<p>
<strong>Escopo:</strong> Este caso de teste é referente ao comando PART<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente Web<br />
<strong>Pré-condições:</strong> Estar em um canal<br />
<strong>Pós-condições:</strong> Sair desse canal <br />
<strong>Entrada:</strong> /part<br />
<strong>Cenário de sucesso(fluxo básico):</strong> - Saiu do canal<br />
<strong>Cenário(s) alternativo(s):</strong> - Mensagem de erro pois não está em um contexto de canal<br />
<strong>Saída obtida(fluxo básico):</strong> Para quem ainda está no canal: (nick) saiu do canal<br />
<strong>Saída obtida(fluxo alternativo):</strong> Canal inválido.<br />
</p>

## Caso de teste CDU04:
<p>
<strong>Escopo:</strong> Este caso de teste é referente ao comando QUIT<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente web<br />
<strong>Pré-condições:</strong> Estar conectado ao servidor<br />
<strong>Pós-condições:</strong> Sair do servidor e voltar para tela de login<br />
<strong>Entrada:</strong> /quit<br />
<strong>Cenário de sucesso(fluxo básico):</strong> - Saiu do servidor<br />
<strong>Saída obtida(fluxo básico):</strong> Saiu do servidor e voltou para a tela de login.<br />
</p>

## Caso de teste CDU05:
<p>
<strong>Escopo:</strong> Este caso de teste é referente ao comando INVITE<br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente web<br />
<strong>Pré-condições:</strong> Estar em um canal, sendo o operador dele caso o canal seja apenas com convite.<br />
<strong>Pós-condições:</strong> Usuário especificado convidado para o canal.<br />
<strong>Entrada:</strong> /invite wilan__<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - O usuário convidado é notificado do convite.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O usuário que realizou o comando não tem permissão de operador do canal que está apenas por convite.<br />
    - O nick passado pelo usuário que realizou o comando não existe.<br />
<strong>Saída obtida(fluxo básico):</strong>(nick) te convidou para o canal (canal). <br />
<strong>Saída obtida(fluxo alternativo 1):</strong> Você precisa ser operador para fazer isso.<br />
<strong>Saída obtida(fluxo alternativo 2):</strong> Nick inválido.<br />
</p>

## Caso de teste CDU06:
<p>
<strong>Escopo:</strong> Este caso de teste é referente ao comando KICK. <br />
<strong>Nível:</strong> Client<br />
<strong>Ator principal:</strong> Cliente web<br />
<strong>Pré-condições:</strong> Estar em um canal e ser operador dele.<br />
<strong>Pós-condições:</strong> O usuário especificado no comando é expulso do canal.<br />
<strong>Entrada:</strong> /kick wilan_<br />
<strong>Cenário de sucesso(fluxo básico):</strong>
    - O usuário especificado é expulso do canal com sucesso.<br />
<strong>Cenário(s) alternativo(s):</strong>
    - O usuário especificado não existe; <br />
    - O usuário que executou o comando não é operador do canal. <br />
<strong>Saída obtida(fluxo básico):</strong> Fecha a aba do canal do qual foi expulso.<br />
<strong>Saída obtida(fluxo alternativo 1):</strong> Nick inválido.<br />
<strong>Saída obtida(fluxo alternativo 2):</strong> Você precisa ser um operador pra fazer isso.<br />
</p>