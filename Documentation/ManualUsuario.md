# Manual do Usuário

## Sumário
* [Introdução] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#1-introdu%C3%A7%C3%A3oo)
* [Conhecendo e acessando o site] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#2-conhecendo-e-acessando-o-site)
    * IRC
    * Requisitos de software e Hardware
* [Comandos] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#3-comandos)
    * [Join] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#31-join)
    * [Part] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#32-part)
    * [Kick] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#33-kick)
    * [Quit] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#34-quit)
    * [Help] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#35-help)
    * [Motd] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#36-motd)
    * [Nick] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#37-nick)
    * [Invite] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#38-invite)
    * [Mode] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#39-mode)
    * [Topic] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#310-topic)
    * [Clear] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#311-clear)
    * [List] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#312-list)

## 1. Introdução
<p> Este documento tem como objetivo ser um guia simples para melhor introduzi-lo no site de chat online desenvolvido como projeto da matéria 
<strong>Aplicações Distribuidas </strong>. 
</p>

## 2. Conhecendo e acessando o site

<p> Esse site eh um chat online que roda em um servidor IRC. <br /><br />

IRC é a sigla de <strong> Internet Relay Chat </strong>, é um protocolo de comunicação utilizado na Internet. Ele é utilizado basicamente como bate-papo (chat) 
e troca de arquivos, permitindo a conversa em grupo ou privada. <br />
Pode acessar o site à partir deste link <strong>http://chatirc.azurewebsites.net/</strong> . <br /><br />

<strong> Requisitos de software e hardware: </strong><br />
O site roda nos principais navegadores disponiveis e no Internet Explorer à partir da versão 6. Infelizmente o único navegador que não roda é o 
Explorer Edge. Os requisitos dos principais navegadores (Mozilla Firefox e Google Chrome): 
</p>

**Chrome:**
* Windows:
    * Windows 7, Windows 8, Windows 8.1, Windows 10 ou versão posterior;
    * Processador Intel Pentium 4 ou posterior compatível com SSE2;

* Mac:
    * OS X Mavericks 10.9 ou versão posterior;

* Linux:
    * Debian 8+, openSUSE 13.3+, Fedora Linux 24+ ou Ubuntu 14.04+ de 64 bits;
    * Processador Intel Pentium 4 ou posterior compatível com SSE2 ;

**Firefox:**
* Windows (32 e 64 bits)
    * Sistemas operacionais: Windows 10, 8, 7, Vista, Server 2003 SP1
    * A versão 64 bits funciona no Windows 7 e posteriores.
    * Hardware mínimo: Pentium 4 ou mais recente que suporte SSE2, 512 MB de RAM, 200MB de espaço em disco

* Mac
    * Sistemas operacionais: Mac OS X 10.11 (El Capitan), 10.10 (Yosemite), 10.9 (Mavericks), 10.8 (Montain Lion), 10.7 (Lion)
    * Hardware mínimo: Computador Macintosh com processador Intel x86, 512MB de RAM, 200MB de espaço em disco

* Linux	
    * Sistema operacional: Linux
    * Requisitos de software: Observe que distribuições Linux podem fornecer pacotes com requisitos diferentes.

![imagem](/Documentation/imagens/Usuário-Tela-Login.png)

*Navegador Mozilla*

<p> Ao acessar o site, poderá se cadastrar colocando um nome e se assim desejar, um nome de canal.
</p> 

## 3. Comandos
<p> Os comandos disponíveis no site estarão listados com breves descrições e com prints de tela 
para melhor representação dos mesmos. 
</p>

### 3.1 Join

<p> Use o comando join para entrar em uma sala. <br />
<strong> /join <canal> </strong>  <br />
Ao criar um novo canal, o usuário é colocado como Administrador do mesmo.
</p>

******

### 3.2 Part 

<p> Use o comando part para sair de um canal. <br />
<strong> /part </strong>
</p>

********

### 3.3 kick 

<p> Use o comando kick para expulsar um usuário de um canal(necessita privilégio de operador).<br /> 
<strong> /kick <canal> <nick> </strong>
</p>

********

### 3.4 Quit 

<p> Use o comando quit para desconectar do servidor e de todos os canais. <br />
<strong> /quit </strong> 
</p>

********

### 3.5 Help 

<p> Use o comando help para exibir a lista de comandos suportados atualmente ou detalhes de determinado comando. <br />
 <strong> /help [comando sem a /] </strong>
</p>

******

### 3.6 Motd

<p> Use o comando motd para exibir a mensagem do dia do servidor. <br />
<strong> /motd </strong>
</p>

********

### 3.7 Nick

<p> Use o comando nick para trocar de nick.  <br />
<strong> /nick <nick> </strong>
</p>

*******

### 3.8 Invite

<p> Use o comando invite para convidar alguém para um canal.  <br />
<strong> /invite <nick> <canal> </strong>
</p>

*********

### 3.9 Mode

<p> Use o comando mode para mostrar os modos atuais do usuário.  <br />
<strong> /mode <nick> </strong>
</p>

*********

### 3.10 Topic

<p> Use o comando topic para adicionar, remover ou exibir um tópico de um canal. <br />
<strong> /topic [topico] </strong>
</p>

*********

### 3.11 Clear

<p> Use o comando clear para limpar o mural de mensagens.  <br />
<strong> /clear </strong>
</p>

*********

### 3.12 List

<p> Use o comando list para mostrar todos os canais do servidor.  <br />
<strong> /list </strong>
</p>