# APLICAÇÕES DISTRIBUÍDAS 

## PROJETO 2 - INTERFACE WEB PARA CHAT IRC

<p> Site oficial do projeto <strong>http://chatirc.azurewebsites.net/</strong> </p>

## DOCUMENTOS DISPONÍVEIS

* [Manual de Usuário] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualUsuario.md#manual-do-usu%C3%A1rio)
* [Manual do Desenvolvedor] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/ManualDesenvolvedor.md#manual-do-desenvolvedor)
* [Casos de Teste] (https://gitlab.com/ad-si-2017-2/p2-g1/blob/master/Documentation/Casos-de-Teste.md#casos-de-teste)

### EQUIPE

Nome | Username GitLab | Função
:-----:|:-----------------:|:-------:
Alex Cruzeiro Alves Gomes | AlexCruzeiro@AlexCruzeiro | Líder/Desenvolvedor
Wilan Carlos da Silva | wilancarlos@wilansilva | Documentador
Jerlianni Barbosa de Oliveira | JerlianniOliveira@Anni-Oliveira | Documentadora
Victor Hugo Viana Mota | VictorMota@victormt4 | Desenvolvedor